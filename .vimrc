colorscheme solarized
syntax on
set tabstop=4
set number
set paste
set background=dark
set hlsearch